<?php
    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url = explode("http://localhost/importer/", $actual_link);

    if($url[1] == ''){
        $url[1] = 'index.php';
    }
?>
<link rel="stylesheet"  href="includes/css/bootstrap.css">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="<?=($url[1] == 'index.php')? 'active' : ''?>"><a href="index.php">Company import</a></li>
      <li class="<?=($url[1] == 'excel_import.php')? 'active' : ''?>"><a href="excel_import.php">Execel import</a></li>
      <li class="<?=($url[1] == 'email-restrict.php')? 'active' : ''?>"><a href="email-restrict.php">Email Restricted</a></li>
      <li class="<?=($url[1] == 'country-update.php')? 'active' : ''?>"><a href="country-update.php">Country Update</a></li>
      <li class="<?=($url[1] == 'import_csv.php')? 'active' : ''?>"><a href="import_csv.php">Import from CSV</a></li>    
    </ul>
  </div>
</nav>