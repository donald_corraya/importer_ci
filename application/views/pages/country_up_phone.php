<style type="text/css">
    form#country_update label{
        height: 74px!important;       
    }
    
    .success_msg{
        border: 1px solid #00000024;
        height: 50px;
        margin-bottom:10px;
        line-height: 50px;
        background: #00800012;
    }
    .error_msg{
        border: 1px solid #00000024;
        height: 50px;
        margin-bottom:10px;
        line-height: 50px;
        background: #ff000017;
    }
</style>

<div class="container-fluid">
    <?php
        if(isset($success)){
    ?>
    <div class="col-lg-12 success_msg">
        <span class=""><?= $success;?></span>
    </div>
    <?php }?>
    
    
    <?php
        if(isset($error_del)){
    ?>
    <div class="col-lg-8 error_msg">
        <span class=""><?= $error_del;?></span>
    </div>
    <?php }?>
    
    <br><br>   
    
    <div class="col-lg-2">
        
        <?php
           echo Country::country_update_menu();
        ?>
        
    </div>
    <div class="col-lg-8 list-inline">
        <h3>Country update by Phone no</h3>
        <form method="post" id="country_update" action="" enctype="multipart/form-data">
            <div class="row">                
                <div class="col-lg-2">
                    <label>Data Table:
                    <select class="form-control" style="width: 100%;">
                        <option value="all_user_data">All User Data</option>                        
                    </select>
                    </label>
                </div>
                <div class="col-lg-2">
                    <label>Phone:
                        <input class="form-control" value="<?= set_value('phone');?>" type="text" name="phone">
                    <span style="font-size: 10px" class="text-danger"><?= form_error("phone")?></span>
                    </label>
                </div>
                
                <div class="col-lg-2">
                    <label>Country Name:
                        <input class="form-control" value="<?= set_value('country');?>" type="text" value="" name="country">
                        <span style="font-size: 10px" class="text-danger"><?= form_error("country")?></span>
                    </label> 
                </div>
                
                <div class="col-lg-12">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

