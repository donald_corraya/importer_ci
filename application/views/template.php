<link rel="stylesheet"  href="<?= base_url()?>assets/css/bootstrap.css">
<link rel="stylesheet"  href="<?= base_url()?>assets/css/custom.css">
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?= base_url()?>">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class=""><a href="index.php">Company import</a></li>
      <li class=""><a href="excel_import.php">Execel import</a></li>
      <li class=""><a href="email-restrict.php">Email Restricted</a></li>
      <li class=""><a href="<?= base_url()?>Country_update">Country Update</a></li>
      <li class=""><a href="import_csv.php">Import from CSV</a></li>    
    </ul>
  </div>
</nav>
<?php

    if(isset($country_update_view)){
        $this->load->view('pages/country_update_view');
    }elseif(isset($country_up_phone)){
        $this->load->view('pages/country_up_phone');   
    }elseif(isset($side_menu_country_update)){
        $this->load->view('pages/side_menu_country_update');   
    }elseif(isset($country_update_email_phone)){
        $this->load->view('pages/country_update_email_phone');   
    }elseif(isset($country_update_city_phone)){
        $this->load->view('pages/country_update_city_phone');   
    }


?>