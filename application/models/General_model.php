<?php

Class General_model extends CI_Model {
    
    
    public function update($arr,$whr,$table) {        
        $this->db->where($whr);
        $result = $this->db->update($table, $arr);
        
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function select($select,$whr ,$table){
        $this->db->select($select);
        $this->db->from($table);
        if(!empty($whr)) {
            $this->db->where($whr);
        }
        
        
        $result = $this->db->get();
        if ($result) {
            return $result->result();
        } else {
            return FALSE;
        }
    }
    
    public function delete($whr,$table) {
        $this->db->delete($table, $whr);
        
        if($this->db->affected_rows() == 1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function insert($table,$attr){
        
	$this->db->insert($table,$attr);
        $id = $this->db->insert_id();
        if($id>0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
}