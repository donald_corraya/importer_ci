<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Country_update extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("General_model");
        $this->load->library('country');
    }

    public function index() {
        $data_is = array();
        $this->form_validation->set_rules('city', 'city', '');
        $this->form_validation->set_rules('address', 'address', '');
        $this->form_validation->set_rules('state', 'state', '');
        $this->form_validation->set_rules('zip', 'zip', '');
        $this->form_validation->set_rules('email', 'email', '');
        $this->form_validation->set_rules('country', 'Country Name', 'required');
        
        if ($this->form_validation->run()) {
            $city = $this->input->post('city');
            $address = $this->input->post('address');
            $state = $this->input->post('state');
            $zip = $this->input->post('zip');
            $email = $this->input->post('email');
            $country = $this->input->post('country');


            if (isset($state) and ! empty($state)) {
                $result = self::data_update($state, $country, "registrant_state");
                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }


            if (isset($city) and ! empty($city)) {
                $result = self::data_update($city, $country, "registrant_city");
                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }
            
            if (isset($address) and ! empty($address)) {
                $result = self::data_update($address, $country, "registrant_address");
                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }


            if (isset($zip) and ! empty($zip)) {
                $result = self::data_update($zip, $country, "registrant_zip");
                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }

            if (isset($email) and ! empty($email)) {
                $result = self::data_update($email, $country, "registrant_email");
                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }
        }
        $data['country_update_view'] = 'country_update_view';
        $this->load->view("template", $data);
    }

    private function data_update($attr, $country, $data_field) {
        
        $arr = array(
            'registrant_country' => $country,
        );
        if ($data_field == 'registrant_email') {
            $whr = "registrant_email LIKE '%" . $attr . "' AND registrant_country=''";
        }
        
        elseif($data_field == 'registrant_phone'){
            $whr ="registrant_country='' AND registrant_phone LIKE '".$attr."%'";           
        }
        
        elseif($data_field == 'registrant_address'){
            $whr ="registrant_country='' AND registrant_address LIKE '%".$attr."%'";
        }
        
        elseif($data_field == "email_phone"){
            $whr = "registrant_country='' AND registrant_email LIKE '%$attr[0]' AND registrant_phone LIKE '$attr[1]%'";
        }
        
        elseif($data_field == "city_phone"){
            $whr = "registrant_country='' AND registrant_city='$attr[0]' AND registrant_phone LIKE '$attr[1]%'";
        }
        else{
            $whr = array(
                'registrant_country' => '',
                $data_field => $attr,
            );
        }
        $check_data = $this->General_model->select("*", $whr, "all_user_data");        
        //echo $this->db->last_query();die;
        
        foreach ($check_data as $cd_check) {
            $id = $cd_check->id;

            $whr = array(
                'registrant_country' => $country,
                'registrant_email' => $cd_check->registrant_email,
            );

            $data_existed = $this->General_model->select("*", $whr, "all_user_data");

            if (!empty($data_existed)) {

                foreach ($data_existed as $data_avail) {
                    $data['error'] = "This data is already exist in all_user_data table " . $data_avail->registrant_country . "-" . $data_avail->registrant_email;

                    $whr = array(
                        'id' => $id
                    );

                    $result = self::delete_data($id, "all_user_data");

                    if ($result == 1) {
                        $data['error_del'] = "Deleted - " . $data_avail->registrant_email . " where ID = " . $id;
                    } else {
                        $data['error_del'] = "Unable to delete data";
                    }
                }
            } else {
                continue;
            }
        }
        
        if ($data_field == 'registrant_email') {
            $whr = "registrant_email LIKE '%" . $attr . "%' AND registrant_country=''";
        } elseif ($data_field == 'registrant_phone') {
            $whr = "registrant_phone LIKE '%" . $attr . "%' AND registrant_country=''";
        }elseif($data_field == 'registrant_address') {
            $whr = "registrant_address LIKE '%" . $attr . "%' AND registrant_country=''";
        }elseif($data_field == "email_phone"){
            $whr = "registrant_country='' AND registrant_email LIKE '%$attr[0]' AND registrant_phone LIKE '$attr[1]%'";
        }elseif($data_field == "city_phone"){
            $whr = "registrant_country='' AND registrant_city='$attr[0]' AND registrant_phone LIKE '$attr[1]%'";
        }
        else{
            $whr = array(
                $data_field => $attr,
                'registrant_country' => ''
            );
        }

        $result = $this->General_model->update($arr, $whr, "all_user_data");

        if ($result) {
            $data['success'] = "Data Updated";
        }
        return $data;
    }

    private function delete_data($id, $table) {

        $whr = array(
            "id" => $id
        );
        $data_store = self::deleted_data_store($id, $table);
        if (!$data_store) {
            $data['error_del'] = "Data did not store in delete table";
            return $data['error_del'];
        }
        $result = $this->General_model->delete($whr, $table);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function deleted_data_store($id, $table) {
        $whr = array(
            'id' => $id
        );
        $get_data = $this->General_model->select("*", $whr, $table);
        foreach ($get_data as $data_arr) {
            $attr = array(
                'table_name' => $table,
                'id_p' => $data_arr->id,
                'resgistrant_name' => $data_arr->resgistrant_name,
                'registrant_company' => $data_arr->registrant_company,
                'registrant_address' => $data_arr->registrant_address,
                'registrant_city' => $data_arr->registrant_city,
                'registrant_state' => $data_arr->registrant_state,
                'registrant_zip' => $data_arr->registrant_zip,
                'registrant_country' => $data_arr->registrant_country,
                'registrant_phone' => $data_arr->registrant_phone,
                'created_at' => $data_arr->created_at,
                'registrant_email' => $data_arr->registrant_email,
                'file_id' => $data_arr->file_id
            );
        }
        $data_reinserted = $this->General_model->insert("delete_history", $attr);
        if ($data_reinserted) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function country_update_by_phone() {
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('country', 'country', 'required');
        if ($this->form_validation->run()) {
            $phone = $this->input->post('phone');
            $country = $this->input->post('country');

            if (isset($phone) and ! empty($phone)) {
                $result = self::data_update($phone, $country, "registrant_phone");

                if (isset($result['error_del'])) {
                    $data['error_del'] = $result['error_del'];
                }
                if (isset($result['success'])) {
                    $data['success'] = "Data Updated";
                }
            }
        }
        $data['country_up_phone'] = 'country_up_phone';
        $this->load->view("template", $data);
    }
    
    public function country_update_email_phone(){
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('country', 'Country Name', 'required');
        
        if ($this->form_validation->run()){
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $country = $this->input->post('country'); 
            
            $attr = array(
                $email,$phone,$country
            );
            
            $result = self::data_update($attr, $country, "email_phone");
            
            if (isset($result['error_del'])) {
                $data['error_del'] = $result['error_del'];
            }
            if (isset($result['success'])) {
                $data['success'] = "Data Updated";
            }
        }
        
        $data['country_update_email_phone'] = 'country_update_email_phone';
        $this->load->view("template", $data);
    }
    
    public function country_update_city_phone(){
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_rules('country', 'Country Name', 'required');
        if ($this->form_validation->run()){
            $city = $this->input->post('city');
            $phone = $this->input->post('phone');
            $country = $this->input->post('country');
            
            $attr = array(
                $city,$phone,$country
            );
            
            $result = self::data_update($attr, $country, "city_phone");
            
            if (isset($result['error_del'])) {
                $data['error_del'] = $result['error_del'];
            }
            if (isset($result['success'])) {
                $data['success'] = "Data Updated";
            }
            
            //$whr = "registrant_country='' AND registrant_city='$city' AND registrant_phone LIKE '$phone%'";
            
            //$check_data = $this->General_model->select("*", $whr, "all_user_data");
            
            
        }
        $data['country_update_city_phone'] = 'country_update_city_phone';
        $this->load->view("template", $data);
    }

}

?>